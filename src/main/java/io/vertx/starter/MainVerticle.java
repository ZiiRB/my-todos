package io.vertx.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class MainVerticle extends AbstractVerticle {

    private static final String MONGO_DB_NAME = "todos-db";
    private static final String MONGO_TODOS_COLLECTION_NAME = "todos";
    private static final AtomicInteger TODOS_ID_COUNTER = new AtomicInteger();

    private MongoClient mongoClient;

    @Override
    public void start(Future<Void> future) {
        mongoClient = MongoClient.createShared(
                vertx, new JsonObject().put("db_name", MONGO_DB_NAME));
        createSampleData();

        Router router = Router.router(vertx);

        router.route("/").handler(StaticHandler.create("assets"));

        router.route("/api/todos*").handler(BodyHandler.create());
        router.get("/api/todos").handler(this::getAll);
        router.post("/api/todos").handler(this::addOne);
        router.put("/api/todos/:id").handler(this::updateOne);
        router.delete("/api/todos/:id").handler(this::deleteOne);

        vertx
                .createHttpServer()
                .requestHandler(router)
                .listen(8080, result -> {
                            if (result.succeeded()) {
                                future.complete();
                            } else {
                                future.fail(result.cause());
                            }
                        }
                );
    }

    private void addOne(RoutingContext routingContext) {
        JsonObject newTodo = routingContext.getBodyAsJson();
        newTodo.put("_id", TODOS_ID_COUNTER.getAndIncrement());
        mongoClient.insert(MONGO_TODOS_COLLECTION_NAME, newTodo, result -> {
            if (result.succeeded()) {
                routingContext.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(newTodo));
            } else {
                result.cause().printStackTrace();
            }
        });
    }

    private void updateOne(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        JsonObject updatedTodo = routingContext.getBodyAsJson();
        if (id == null || updatedTodo == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Integer idAsInteger = Integer.valueOf(id);
            JsonObject updateQuery = new JsonObject().put("_id", idAsInteger);
            JsonObject update = new JsonObject().put("$set", updatedTodo);
            mongoClient.update(MONGO_TODOS_COLLECTION_NAME, updateQuery, update, result -> {
                if (result.succeeded()) {
                    routingContext.response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(Json.encodePrettily(updatedTodo));
                        } else {
                    result.cause().printStackTrace();
                        }
                    }
            );
        }
    }

    private void deleteOne(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Integer idAsInteger = Integer.valueOf(id);
            JsonObject query = new JsonObject().put("_id", idAsInteger);
            mongoClient.remove(MONGO_TODOS_COLLECTION_NAME, query, result -> {
                if (result.succeeded()) {
                            routingContext.response().setStatusCode(204).end();
                        } else {
                    result.cause().printStackTrace();
                        }
                    }
            );
        }
    }

    private void getAll(RoutingContext routingContext) {
        JsonObject query = new JsonObject();
        FindOptions options = new FindOptions().setSort(new JsonObject().put("_id", 1));
        mongoClient.findWithOptions(MONGO_TODOS_COLLECTION_NAME, query, options, result -> {
            if (result.succeeded()) {
                        routingContext.response()
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(Json.encodePrettily(result.result()));
                    } else {
                result.cause().printStackTrace();
                    }
                }
        );
    }

    private void createSampleData() {
        mongoClient.dropCollection(MONGO_TODOS_COLLECTION_NAME, dropResult -> {
            if (dropResult.succeeded()) {
                Stream.of(
                        createTodo("Need to learn Router", "ATMDT"),
                        createTodo("Need to learn RoutingContext", "ATMDT")
                ).forEach(mongoDoc ->
                        mongoClient.insert(MONGO_TODOS_COLLECTION_NAME, mongoDoc, insertResult -> {
                            if (insertResult.failed()) {
                                insertResult.cause().printStackTrace();
                            }
                        })
                );
            } else {
                dropResult.cause().printStackTrace();
            }
        });
    }

    private JsonObject createTodo(String name, String details) {
        return new JsonObject()
                .put("_id", TODOS_ID_COUNTER.getAndIncrement())
                .put("name", name)
                .put("details", details);
    }
}
